import * as THREE from 'three';

/**
 * Maze class
 * @param wrapper
 * @param button
 */
class ThreeMaze
{
    constructor(window, wrapper, width, height) {
        // Object attributes
        this.window = window
        this.wrapper = wrapper;
        this.camera = {};
        this.scene = {};
        this.materials = {};
        this.map = [];
        this.renderer = {};
        this.end = {};
        this.width = width;
        this.height = height;
        this.thickness = 30;     
    }

    generateEmptyMap = (width, height) => {
        var map = [];
    
        for (var x = 1; x <= width; x += 1)
        {
            map[x] = [];
            for (var y = 1; y <= height; y += 1)
            {
                map[x][y] = 0;  
            }
        }
    
        return map;
    }
    
    onGenerateMaze(map_representation)
    {
        var new_map = this.generateEmptyMap(this.width, this.height);
        
        for (var x = this.width; x > 0; x -= 1)
        {
            for (var y = 1; y < this.height + 1; y += 1)
            {      
                // store current kind of cell
                new_map[x][y] = {
                    'kind': map_representation[x][y] // block or player
                }

                // check if necessary to update (need to be improved => some blocks can be damaged)
                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined' 
                    && new_map[x][y].kind === this.map[x][y].kind) {

                    new_map[x][y] = this.map[x][y];
                    continue;

                } else if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined') {
                    
                    // check if player and take care in this case of the two mesh
                    if (this.map[x][y].player !== undefined) {
                        
                        this.map[x][y].player.visible = false;
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].player);
                        this.scene.remove(this.map[x][y].mesh);
                    }
                    else {
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                    }
                }

                // Adds a new mesh if needed
                if (map_representation[x][y] === "BORDER")
                {
                    // Generates the mesh for border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                
                }
                else if (map_representation[x][y] === "GROUND") {
                     // generate the mesh with ground
                     var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                     new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                     new_map[x][y].mesh.visible = true;
                     new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                     this.scene.add(new_map[x][y].mesh);
                }
                else if (map_representation[x][y] === "HIDER")
                {   
                    // Add hider
                    new_map[x][y].player = new THREE.Mesh(new THREE.SphereGeometry(this.thickness / 2, 9, 9), this.materials.hider)

                    new_map[x][y].player.visible = true;
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } 
                else if (map_representation[x][y] === "SEEKER")
                {   
                    // Add seeker
                    new_map[x][y].player = new THREE.Mesh(new THREE.SphereGeometry(this.thickness / 2, 9, 9), this.materials.seeker)

                    new_map[x][y].player.visible = true;
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } 
                else {
                    // by default other border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                }
            }
        }


        this.map_representation = map_representation;
        this.map = new_map;
    };
 
    /**
     * Inits the scene
     */
    initScene()
    {
        // Scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);

        // Materials
        const loader = new THREE.TextureLoader();

        const ground_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://tse2.mm.bing.net/th?id=OIP.gnc6dyhXu6CUF_lBJ5b7KwHaHa&pid=Api'),
        })

        const wall_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://tse2.mm.bing.net/th?id=OIP.rgCGU0PU4_fcq6sVeGJyZgHaE8&pid=Api'),
            wireframe: false
        })
        
        this.materials =
        {
            grey: wall_material,
            ground: ground_material,
            hider: new THREE.MeshLambertMaterial({color: 0x0066ff}),
            seeker: new THREE.MeshLambertMaterial({color: 0x990000}),
            orange: new THREE.MeshLambertMaterial({color: 0xbe842b}),
        };

        // Camera
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera.position.x = 1000;
        this.camera.position.y = 800;
        this.camera.position.z = -300;

        this.camera.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, 0.5, 1);
        this.scene.add(directional);

        // this.camera.lookAt(this.scene.position);
        // Renderer
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
    };

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    render()
    {
        requestAnimationFrame(this.render.bind(this));
        
        this.camera.lookAt(new THREE.Vector3(0, -150, 0));
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.render(this.scene, this.camera);
    };

    /**
     * Sets the scene dimensions on window resize
     */
    onWindowResize()
    {
        // Check if there is a better way to adjust width and height
        var width = (this.window.innerWidth / 12) * 8.5 || this.window.document.body.clientWidth;
        var height = this.window.innerHeight - 120 || this.window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    };
}

export default ThreeMaze;