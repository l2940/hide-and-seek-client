import ThreeMaze from './maze.js'
import Alpine from 'alpinejs'
import {SERVER_URL} from './params.js'

var maze = undefined;
const wrapper = document.querySelector('.three');

const load = () => {
    
    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game?id=${id}`)
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = response.status;
                return Promise.reject(error);
            }

            // remove info message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        .then(data => {
            
            // initialize game
            wrapper.innerHTML = ""
            Alpine.store('gameEnd', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);


            let width = data['width']
            let height = data['height']

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);
            
            Alpine.store('gameProgress', percentProgress);

            maze = new ThreeMaze(window, wrapper, width, height);
    
            // Inits
            maze.initScene();
            maze.onWindowResize();
            maze.render();

            var map = [];
            let counter = 0;

            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];

                for (var y = 1; y <= height; y += 1)
                {   
                    if ('block' in data['cells'][counter]) {

                        map[x][y] = data['cells'][counter]["block"]["kindStr"]
                    }
                    if ('player' in data['cells'][counter]) {
                        
                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                    }
                    counter++;
                }
            }
            
            // load maze map
            maze.onGenerateMaze(map);

            window.addEventListener('resize', maze.onWindowResize.bind(maze));
        }).catch(e => {
            console.log(e)
            Alpine.store('gameInfo', "Game not found...");
            Alpine.store('gameError', true);
        });
}

const updateLoop = () => {
    
    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game/step`, {
            method: 'POST', 
            body: JSON.stringify({"id": id})
        })
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = res.status;
                return Promise.reject(error);
            }

            // remove error message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        .then(data => {
            
            // no server error currently
            Alpine.store('gameError', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);

            let width = data['width']
            let height = data['height']
            console.log(data['round'], 'of', data['maxRounds'], '=> end? : ', data['end']);
            let gameEnd = data['end'];

            Alpine.store('gameEnd', gameEnd);

            if (gameEnd) {
                // clearInterval(canvasInterval);
                let currentWinner = data['winnerStr'];
                Alpine.store('gameInfo',`Game ${Alpine.store("gameId")} is finished! Winner is: ${currentWinner}`)
                Alpine.store('gameWinner', currentWinner);
            }

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            var map = [];
            let counter = 0;

            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];

                for (var y = 1; y <= height; y += 1)
                {   
                    if ('block' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["block"]["kindStr"]
                    }
                    if ('player' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                    }
                    counter++;
                }
            }
            
            // reload maze map
            if (maze !== undefined) {
                maze.onGenerateMaze(map);
            }
        })
        .catch((e) => {
            console.log(e)

            Alpine.store('gameInfo', "Server cannot be reached");
            Alpine.store('gameError', true);
        });
}

export {load, updateLoop};